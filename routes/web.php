<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/contact', function () {
    return view('contactus');
});
Route::get('/login', function () {
    return view('login');
});
Route::get('/reset', function () {
    return view('forgotpass');
});
Route::get('/register', function () {
    return view('register');
});
Route::get('/', 'frontendcontroller@home');
Route::get('/notfound', 'frontendcontroller@notfound');
Route::get('/recovery/{email}/{token}', 'Authcontroller@setnewpassword');
Route::get('/addressactivation/{id}/{token}', 'Authcontroller@activateWallet');
Route::get('{path}',"frontendcontroller@index")->where( 'path', '([A-z\d\-\/_.]+)?' );