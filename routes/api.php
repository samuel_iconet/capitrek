<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/register', 'Authcontroller@register');
Route::get('/activate/{user_id}/{remember_token}', 'Authcontroller@activateAccount');
Route::get('/adminauth/error','Authcontroller@adminAuthError');
Route::get('/test','AuthUserController@test');

// investment 

Route::post('/invest', 'investementController@invest');

//validate session
Route::get('/validateSession', 'userController@validateSession');
Route::post('/recover/password', 'Authcontroller@recover');
Route::post('/recoverpassword/save', 'Authcontroller@savePassword');

//user dashboard
Route::get('/userdashboard', 'userDashboardController@getDashboardData');

//user wallet 
Route::post('/addwallet', 'userWalletController@addwallet');
Route::post('/deletewallet', 'userWalletController@deletewallet');
Route::get('/resendlink/{id}', 'userWalletController@resendlink');

//user Data 
Route::post('/updatedata', 'userController@updateData');

//placeWithdrawal
Route::post('/placeWithdrawal', 'investementController@placeWithdrawal');
Route::get('/cancelWithdrawal/{id}', 'investementController@cancelWithdrawal');
// auth support ticket 
Route::get('/support/ticket', 'authTicketController@getTickets');
Route::post('/support/ticket/create', 'authTicketController@createTickets');
Route::post('/support/ticket/reply', 'authTicketController@replyTickets');
Route::get('/support/ticket/markread/{ticket_id}', 'authTicketController@markRead');
// admin support ticket 
Route::post('/admin/ticket/reply', 'adminTicketController@replyTickets');
Route::get('/admin/ticket/close/{ticket_id}', 'adminTicketController@closeTicket');
Route::get('/admin/ticket', 'adminTicketController@getTickets');

//payments

Route::post('/createDeposit', 'paymentController@createDeposit');
Route::get('/payment/confirm/{token}/{id}', 'Authcontroller@comfirmPayment');
Route::get('/payment/cancel/{token}/{id}', 'Authcontroller@cancelPayment');


// credit payments 

Route::get('/creditpayments/{code}', 'Authcontroller@creditInvestments');
