<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class post extends Model
{
    //
    public function comments()
    {
        return $this->hasMany(comment::class);

    }
    public function user()
    {
        return $this->belongsTo(user::class);
    }
}
