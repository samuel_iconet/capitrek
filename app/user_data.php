<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class user_data extends Model
{
    //
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
