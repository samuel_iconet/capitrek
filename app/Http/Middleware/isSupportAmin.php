<?php

namespace App\Http\Middleware;

use Closure;

class isSupportAmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::User()->role == 'support_admin'){
            return $next($request);
        }else{
            return redirect('/api/adminauth/error');
        }
    }
}
