<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;
use App\investment;
use App\investment_type;
use App\withdrawal;
use Carbon\Carbon;

use App\Mail\notificationMail;
use Illuminate\Support\Facades\Mail;
use Auth;
class investementController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:api');
        // $this->middleware('admin');

    }
    public function cancelWithdrawal($id){
      $account = Auth::User()->account;
      $user = Auth::User();
      $withrawal = Auth::User()->withdrawal->where('id' , $id)->first();
      $withrawal->status = 'Cancelled';
      $account->available_balance =  $account->available_balance +  $withrawal->amount;
      $account->pending_balance =  $account->pending_balance -  $withrawal->amount;
      $withrawal->save();
      $account->save();
      $notification = "Your have Successfully cancelled a withdrawal Request with amount $".$withrawal->amount;
      Mail::to($user)->send(new notificationMail($user ,$notification));
      $response['code']  = "200";
    return response()->json($response ,200);
    }
    public function placeWithdrawal(request $request){
      $validator = Validator::make($request->all(), [
        "amount" =>  "required",
        'address_id' => 'required',
       
  ]);

  if ($validator->fails()) {

       return $validator->messages();
  }
  $user = Auth::User();
  $account = Auth::User()->account;
  if($request->amount > $account->available_balance){
    $notification = "Your withdrawal request for $".$request->amount." was not successfull due to insufficient funds, your available withdrawable balance is $".$account->available_balance;
    Mail::to($user)->send(new notificationMail($user ,$notification));
    $response['error'] = "Insufficient Funds";
    $response['code']  = "401";
    return response()->json($response ,200);
  }else{
    $withrawal = new withdrawal;
    $withrawal->amount =  $request->amount;
    $withrawal->user_id =  Auth::User()->id;
    $withrawal->address_id =  $request->address_id;
    $withrawal->status = "Pending";
    $account->available_balance =  $account->available_balance -  $request->amount;
    $account->pending_balance =  $account->pending_balance +  $request->amount;
    $withrawal->save();
    $account->save();
    $notification = "Your withdrawal request for $".$request->amount." was successfull, you would be notified when withdrawal is complete. ";
    Mail::to($user)->send(new notificationMail($user ,$notification));
    $response['code']  = "200";
    return response()->json($response ,200);
  }

    }
    public function invest(request $request){
        $validator = Validator::make($request->all(), [
            "amount" =>  "required",
            'type' => 'required',
           
      ]);

      if ($validator->fails()) {

           return $validator->messages();
      }
      $user  = Auth::User();
      $account =  $user->account;
      if(isset($account) && $account->available_balance >= $request->amount){
        $investment  = new investment;
        $investment->user_id = $user->id;
        $investment->amount = $request->amount;

        $investment_type  = investment_type::findOrFail($request->type);

        $percentage = $investment_type->percentage;
        $days = $investment_type->day_count; 

        // if($request->type = 'monthly'){
        //  $percentage = 10; 
        //  $days = 28;  
        // }else if($request->type = 'quarterly'){
        //     $percentage = 40; 
        //      $days = 28*3;  

        // }
        // else if($request->type = 'halfyear'){
        //     $percentage = 100; 
        //     $days = 28*6; 
        // }
        $investment->interrest_due = $percentage /100 *$request->amount;
        $investment->interrest_paid = 0;
        $investment->type = $investment_type->name;
        $investment->duration = $investment_type->duration;
        $investment->percentage = $investment_type->percentage;
        $investment->percentage = $investment_type->percentage;

        $current = Carbon::now();
        $investment->due_date = $current->addDays($investment_type->day_count);
        $investment->daily_reward = $investment->interrest_due/$days;
        $investment->status = 'active';
        $investment->save();
        $account->available_balance  =   $account->available_balance - $request->amount;
        $account->locked_balance  =   $account->locked_balance + $request->amount;
        $account->save();
        $user = Auth::User();
        $notification = "Your have successfully invested $".$investment->amount." in a ".$investment_type->name." investment plan, with a ".$investment_type->percentage."% interrest rate which would be due on ".$investment->due_date;
    Mail::to($user)->send(new notificationMail($user ,$notification));
        $response['code']  = "200";
        $response['account']  = $account;
        return response()->json($response ,200);
      }else{
        $response['error'] = "Insufficient Funds";
        $response['code']  = "401";
        return response()->json($response ,200);
      }
    }

}
