<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
class AuthUserController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:api');
        // $this->middleware('admin');

    }
    public function test(){
        return Auth::User()->wallets;
    }
}
