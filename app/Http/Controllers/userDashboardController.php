<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\investment_type;
class userDashboardController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:api');

    }
    public function getDashboardData(){
      $user =  Auth::User();//->with('account')->with('data')->with('transaction')->with('withdrawal')->with('investment')->with('wallets');
      $user['account'] = $user->account;
      $user['data'] = $user->data;
      $user['withdrawal'] = $user->withdrawal;
      $user['investment'] = $user->investment;
      $user['wallets'] = $user->wallets;
     
      $investmentType = investment_type::where('status' , '1')->get();
    
       $response['code'] = 200;
       $response['user'] = $user;
       $response['investment_type'] = $investmentType;
       return response()->json($response ,200);
       
        
    }
}
