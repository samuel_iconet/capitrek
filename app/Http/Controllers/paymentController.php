<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Str;
use App\transaction;
use Auth;
use Validator;
use Redirect;
use App\Mail\notificationMail;
use Illuminate\Support\Facades\Mail;
class paymentController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:api');

    }
   
    public function createDeposit(request $request){
        $validator = Validator::make($request->all(), [
            "amount" =>  "required",
           
      ]);

      if ($validator->fails()) {

           return $validator->messages();
      }
      $user = Auth::User();
       $deposit = new transaction;
       $deposit->amount = $request->amount;
       $deposit->user_id = $user->id;
       $deposit->description = 'deposit';
       $deposit->token = Str::random(40);
       $deposit->ref_id = '0';
       $deposit->type = 'deposit';
       $empty['status'] = 'empty';
       $deposit->data = serialize($empty);
       $deposit->status = 'Pending Payment';
       $deposit->save();
       $data['name'] = 'deposit';
       $data['description'] = 'deposit Request';
       $data['local_price']['amount'] = $request->amount;
       $data['local_price']['currency'] = "USD";
       $data['pricing_type'] = "fixed_price";
       $data['metadata']['customer_id'] = $user->id;
       $data['metadata']['customer_name'] = $user->name;
       $data['redirect_url'] = url('/').'/api/payment/confirm/'.$deposit->token.'/'.$deposit->id;
       $data['cancel_url'] = url('/').'/api/payment/cancel/'.$deposit->token.'/'.$deposit->id;

     

       
       
       $curl = curl_init();
       
       curl_setopt_array($curl, array(
           CURLOPT_URL => "https://api.commerce.coinbase.com/charges",
           CURLOPT_RETURNTRANSFER => true,
           CURLOPT_ENCODING => "",
           CURLOPT_MAXREDIRS => 10,
           CURLOPT_TIMEOUT => 30000,
           CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
           CURLOPT_CUSTOMREQUEST => "POST",
           CURLOPT_POSTFIELDS => json_encode($data),
           CURLOPT_HTTPHEADER => array(
               // Set here requred headers
               "accept: */*",
               "X-CC-Version: 2018-03-22",
               'Content-Type: application/json',
               'X-CC-Api-Key: 33c3d878-1478-42db-bcc9-b79026b0018b'
           ),
       ));
       
       $response = curl_exec($curl);
       $err = curl_error($curl);
       
       curl_close($curl);
       
       if ($err) {
          
           $res = json_decode($err);
           $responce['code']  = "400";
           $responce['error']  = $res;
         return response()->json($responce ,200);
       } else {
         $res = json_decode($response, true);
        // 
        $url = $res['data']['hosted_url'];
        $code = $res['data']['code'];
        $deposit->ref_id =$code ;
        $deposit->data =serialize($res);
        $deposit->redirect =$url;
        $deposit->save();
        $user =Auth::User();
        $notification = "You have created a deposit request of $".$deposit->amount." please proceed to make payment, Payments are automaically confirmed after payment.";
        Mail::to($user)->send(new notificationMail($user ,$notification));
         $responce['code']  = "200";
         $responce['redirect']  =$url;
       return response()->json($responce ,200);
        }
       


    }
    
    public function test(){
        // $data = '{
        //     "name": "The Sovereign Individual",
        //     "description": "Mastering the Transition to the Information Age",
        //     "local_price": {
        //       "amount": "100.00",
        //       "currency": "USD"
        //     },
        //     "pricing_type": "fixed_price",
        //     "metadata": {
        //       "customer_id": "id_1005",
        //       "customer_name": "Satoshi Nakamoto"
        //     },
        //     "redirect_url": "https://charge/completed/page",
        //     "cancel_url": "https://charge/canceled/page"
        //   }';
        
        // $data1 = json_decode($data);

        $data['name'] = 'username';
        $data['description'] = 'deposit Request';
        $data['local_price']['amount'] = "100";
        $data['local_price']['currency'] = "USD";
        $data['pricing_type'] = "fixed_price";
        $data['metadata']['customer_id'] = "USD";
        $data['metadata']['customer_name'] = "USD";
        $data['redirect_url'] = "https://charge/completed/page";
        $data['redirect_url'] = "https://charge/canceled/page";

      

        
        
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.commerce.coinbase.com/charges",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                // Set here requred headers
                "accept: */*",
                "X-CC-Version: 2018-03-22",
                'Content-Type: application/json',
                'X-CC-Api-Key: 33c3d878-1478-42db-bcc9-b79026b0018b'
            ),
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        
        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            print_r($response);
        }

        
    }
}
