<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\ticket;
use App\ticket_message;
use App\User;
use Validator;
class authTicketController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:api');

    }
    public function getTickets(){
    $tickets = ticket::where('user_id' , Auth::User()->id)->orderBy('id', 'DESC')->get();
    if(count($tickets) > 0){
        foreach($tickets as $ticket){
            $ticket['messages'] = ticket_message::where('ticket_id' , $ticket->id)->get();
            if(count( $ticket['messages']) > 0){
                foreach($ticket['messages'] as $message){
                    $message['user'] = User::find($message->sender_id);
                }  
            }
            $ticket['unread'] = count(ticket_message::where(['ticket_id' => $ticket->id , 'status' => 'unread'])->where('sender_id' , '<>' , Auth::User()->id)->get());
        }
    }
    $response['code'] = 200;
    $response['tickets'] = $tickets;
    return response()->json($response ,200);
    }
    public function createTickets(request $request){
        $validator = Validator::make($request->all(), [
            "title" =>  "required",
            "message" =>  "required",

        
    ]);
  
    if ($validator->fails()) {
  
         return $validator->messages();
    }
    $ticket =  new ticket;
    $ticket->title  = $request->title;
    $ticket->user_id  = Auth::User()->id;
    $ticket->status  = 'Pending';
    $ticket->save();

    $message =  new ticket_message;
    $message->ticket_id = $ticket->id;
    $message->sender_id = Auth::User()->id;;
    $message->message = $request->message;
    $message->status = 'unread';
    $message->save();

    $response['code'] = 200;
    return response()->json($response ,200);

    }
    public function replyTickets(request $request){
           $validator = Validator::make($request->all(), [
            "ticket_id" =>  "required",
            "message" =>  "required",

        
    ]);
  
    if ($validator->fails()) {
  
         return $validator->messages();
    }
    $ticket =   ticket::findOrFail($request->ticket_id);
  

    $message =  new ticket_message;
    $message->ticket_id = $ticket->id;
    $message->sender_id = Auth::User()->id;;
    $message->message = $request->message;
    $message->status = 'unread';
    $message->save();
    
    $response['code'] = 200;
    return response()->json($response ,200);

    }
    public function markRead($tiket_id){
    $ticket =   ticket::findOrFail($tiket_id);
    $messages = ticket_message::where(['ticket_id' => $ticket->id , 'status' => 'unread'])->where('sender_id' , '<>' , Auth::User()->id)->get();

    if(count( $messages) > 0){
        foreach($messages as $message){
           $message->status = 'read';
           $message->save();

        }  
    }
    $response['code'] = 200;
    return response()->json($response ,200);

    }

}
