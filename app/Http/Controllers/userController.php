<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Validator;
use App\user;
use App\user_data;
use App\Mail\recoveryMail;
use Illuminate\Support\Facades\Mail;
class userController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:api');

    }
    public function recover(request $request){
        $user = User::where('email' , $request->email)->first();
        if(isset($user)){
            $recoverykey = Str::random(40);
            $user->remember_token = $recoverykey;
            $user->save();
             Mail::to($user)->send(new recoveryMail($user));
            $response['code'] = 200;
            return response()->json($response ,200);
        }else{
            $response['code'] = 401;
            $response['error'] = "Email not registered!!!";
            return response()->json($response ,200);   
        }
    }
    public function validateSession(){
     if(Auth::User()->status == '1'){
        $response['code']  = "200";
        $response['user'] = Auth::User();
        return response()->json($response ,200);
     } 
      $response['code']  = "400";
     return response()->json($response ,200);
    }
    public function updateData(request $request){
        $validator = Validator::make($request->all(), [
            "file" =>  "required",
            "country" =>  "required",
            "state" =>  "required",
            "address" =>  "required",
      ]);

      if ($validator->fails()) {

           return $validator->messages();
      }
      $user_data = Auth::User()->data; 
      if($user_data == null){
          $user_data = new user_data;
          $user_data->user_id = Auth::User()->id;
      }
      $user_data->state = $request->state;
      $user_data->address = $request->address;
      $user_data->country = $request->country;

      
     if ($request->has('file')) {
        $image = $request->file('file');
        $name = 'profile'.'_'.time();
        $filePath =  $name. '.' . $image->getClientOriginalExtension();
        $request->file->storeAs('profile', $filePath, 'public');
        $profile = $filePath;
         $user_data->profile = $profile;
 
    }
    $user_data->save();
    $response['code']  = "200";
    return response()->json($response ,200);
    }
}
