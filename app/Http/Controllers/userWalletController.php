<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\wallet;
use Auth;
use Validator;
use Str;
use App\Mail\addressActivationMail;
use Illuminate\Support\Facades\Mail;
class userWalletController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:api');

    }
    public function deletewallet(request $request){
        $validator = Validator::make($request->all(), [
            "id" =>  "required",
      ]);

      if ($validator->fails()) {

           return $validator->messages();
      } 
      
      $wallet = Auth::User()->wallets->where('id' , $request->id)->first();
      $wallet->delete();
      $response['code'] = 200;
      return response()->json($response ,200); 
    }
    public function addwallet(request $request){
        $validator = Validator::make($request->all(), [
            "type" =>  "required",
            'address' => 'required',
           
      ]);

      if ($validator->fails()) {

           return $validator->messages();
      }
      $wallet  = new wallet;
      $wallet->type = $request->type;
      $wallet->address = $request->address;
      $wallet->status = 0;
      $wallet->token =  Str::random(40);
      $wallet->user_id = Auth::User()->id;
      $wallet->save();
      $user = Auth::User();
      Mail::to($user)->send(new addressActivationMail($user ,$wallet));
      $response['code'] = 200;
      return response()->json($response ,200);


    }
    public function resendlink($id){
       $wallet = wallet::findOrFail($id);
        $user = $wallet->user;
        Mail::to($user)->send(new addressActivationMail($user ,$wallet));
        $response['code'] = 200;
        return response()->json($response ,200); 
    }
   
}
