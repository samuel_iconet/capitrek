<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;
use Str;
use App\accountmanager;
use App\transaction;
use App\investment;
use App\account;
use App\wallet;
use App\Mail\notificationMail;
use App\Mail\welcomeMail;
use App\Mail\accountActivation;
use App\Mail\recoveryMail;
use Illuminate\Support\Facades\Mail;
class Authcontroller extends Controller
{
    //
    public function register(request $request){
        $validator = Validator::make($request->all(), [
            "username" =>  "required",
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|',
            'phone' => 'required',
            'code' =>  'required',
      ]);

      if ($validator->fails()) {

           return $validator->messages();
      }
      
      $user = new User;
      $recoverykey = Str::random(40);
      $user->remember_token = $recoverykey;
      $user->name = $request->username;
      $user->email = $request->email;
      $user->code = $request->code;
      $user->phone = $request->phone;
      $user->role ='user';
      $user->status ='0';
      $user->password = bcrypt($request->password);
       $user->save();
      $accountmanager   = new accountmanager;
      $accountmanager->generateAccount($user);
      Mail::to($user)->send(new welcomeMail($user));
      Mail::to($user)->send(new accountActivation($user));
       $response['code'] = 200;
        return response()->json($response ,200);

    }
    public function activateAccount($user_id , $remember_token){
        $user = User::where(['id' => $user_id  , 'remember_token' => $remember_token])->first();
        if(isset($user)){
            $user->status = '1';
            $user->remember_token = '';
            $user->save();
            $notification = "Your account have been activated successfully please proceed to login.";
            Mail::to($user)->send(new notificationMail($user ,$notification));
           
        } return redirect('/login');
    }
    public function savePassword(request $request){
        $validator = Validator::make($request->all(), [
            "token" =>  "required",
            'email' => 'required|string|email',
            'password' => 'required|string|min:6|'
      ]);

      if ($validator->fails()) {

           return $validator->messages();
      }
        $user = User::where(['email' => $request->email , 'remember_token' => $request->token])->first();
        if(isset($user)){
           $user->password = bcrypt($request->password);
           $user->remember_token = NULL;
           $user->save();
           $notification = "Your password was changed successfully.";
            Mail::to($user)->send(new notificationMail($user ,$notification));
           $response['code'] = 200;
          return response()->json($response ,200);

        }else{
            $response['error'] = "invalid token" ;
            $response['code'] = 303;
           return response()->json($response ,200);

        }
   }
    public function recover(request $request){
        $user = User::where('email' , $request->email)->first();
        if(isset($user)){
            $recoverykey = Str::random(40);
            $user->remember_token = $recoverykey;
            $user->save();
             Mail::to($user)->send(new recoveryMail($user));
            $response['code'] = 200;
            return response()->json($response ,200);
        }else{
            $response['code'] = 401;
            $response['error'] = "Email not registered!!!";
            return response()->json($response ,200);   
        }
    }
    public function setnewpassword($id , $token){
        $user = User::where(['id' => $id , 'remember_token' => $token])->first();
        if(isset($user)){
         return view('resetpassword' , ['user' => $user ]); 
 
        }
        return view('resetpassword' , ['error' => "Invalid Recovery Token" ]);
    }
    public function comfirmPayment($token , $id){
        $transaction  =  transaction::where(['id' => $id , 'token' => $token])->first();
        if(isset($transaction) && $transaction->status != 'Cancelled'){
            $user = user::findOrFail($transaction->user_id);
            $account = $user->account;
            $account->available_balance =$account->available_balance + $transaction->amount;
            $account->save();
            $transaction->status = 'Successfull';
            $transaction->token = 'Successfull';
            $transaction->save();
            $notification = "Your payment of $".$transaction->amount." was successfully recieved. Your account have been credited successfully.";
            Mail::to($user)->send(new notificationMail($user ,$notification));
            $response['code'] = 200;
            return response()->json($response ,200);
        }else{
            $response['code'] = 401;
            $response['error'] = "Invalid details!!!";
            return response()->json($response ,200);   
        }
    }
    public function cancelPayment($token , $id){
        $transaction  =  transaction::where(['id' => $id , 'token' => $token])->first();
        if(isset($transaction)){
            
            $transaction->status = 'Cancelled';
            $transaction->token = 'Cancelled';
            $transaction->save();
            $notification = "Your payment of request for $".$transaction->amount." was successfully cancelled. ";
            Mail::to($user)->send(new notificationMail($user ,$notification));
           
        }
        return redirect('/dashboard');
    }
    public function adminAuthError(){
        $response['error'] = "User Not Permitted";
        $response['code']  = "401";
        return response()->json($response ,200);
    }
    public function creditInvestments($code){
        $dedicated_code = "34343434";
        if($code == $dedicated_code){
            $investments = investment::where('status'  , 'active')->get();
            foreach($investments as $investment){
                $user = $investment->user;
                $account = $user->account;
                if($investment->interrest_due > $investment->interrest_paid){
                   
                    $account->available_balance = $account->available_balance + $investment->daily_reward;

                    $investment->interrest_paid = $investment->interrest_paid + $investment->daily_reward;
                    $transaction = new transaction;
                    $transaction->amount = $investment->daily_reward;
                    $transaction->user_id = $user->id;
                    $transaction->description = 'Daily Investment Interrest';
                    $transaction->token = 'nul';
                    $transaction->ref_id = $investment->id;
                    $transaction->type = 'Interrest Credit';
                    $empty['status'] = 'empty';
                    $transaction->data = serialize($empty);
                    $transaction->status = 'Successful';
                   
                    $transaction->save();
                    $investment->save();
                    $account->save();
                    

                }else{
                    $investment->status = 'Completed'; 
                    $account->available_balance = $account->available_balance + $investment->amount;
                    $investment->save();
                    $account->save();

                   
                }
            }
        $response['code'] = 200;
        return response()->json($response ,200);
    }else{
        $response['code'] = 401;
        $response['error'] = "Invalid code!!!";
        return response()->json($response ,200);   
    }
    }
    public function activateWallet($id ,$token ){
        $wallet = wallet::where(['id'  => $id , 'token' => $token])->first();
        if(isset($wallet)){
            $wallet->status = '1';
            $wallet->save();
            
        }
        return  redirect('/settings');
    }
}
