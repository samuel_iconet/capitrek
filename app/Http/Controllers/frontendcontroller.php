<?php

namespace App\Http\Controllers;
use App\post;
use App\comment;
use Illuminate\Http\Request;

class frontendcontroller extends Controller
{
    //
    public function index(){
        return view('dashboard');
    }
    public function notfound(){
    return view('404');
    }
    public function home(){
    $posts = post::where('status'  , '1')->get();
    foreach($posts as $post){

        $post['comments'] = $post->comments;
        foreach($post['comments'] as $comment ){
            $post['comments']['user'] =   $comment->user;
        }
        $post['user'] = $post->user;
    } 
    return view('welcome' , ['posts' => $posts]);
    }
}
