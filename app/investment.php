<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class investment extends Model
{
    //
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
