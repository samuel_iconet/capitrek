<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class transaction extends Model
{
    //
    protected $hidden = [
        'token'
    ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
