<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function account()
    {
        return $this->hasOne(account::class);
    }
    public function data()
    {
        return $this->hasOne(user_data::class);
    }
    public function wallets()
    {
        return $this->hasMany(wallet::class);

    }
    public function transaction()
    {
        return $this->hasMany(transaction::class);

    }
    public function ticket()
    {
        return $this->hasMany(ticket::class);

    }
    public function withdrawal()
    {
        return $this->hasMany(withdrawal::class);

    }
    public function investment()
    {
        return $this->hasMany(investment::class);

    }
    
}
