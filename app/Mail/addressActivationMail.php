<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\User;
use App\wallet;
class addressActivationMail extends Mailable
{
    use Queueable, SerializesModels;
    public $user;
    public $wallet;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user , wallet $wallet)
    {
        //
        $this->user = $user;
        $this->wallet = $wallet;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('Mail.addressActivation');
    }
}
