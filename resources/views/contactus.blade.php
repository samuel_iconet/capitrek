@extends('layouts.homelayout')


@section('content')
<div class="page-title-section section section-padding-top" data-overlay="0.7" data-bg-image="assets/images/bg/breadcrumb-bg-five.jpg">
            <div class="page-title">
                <div class="container">
                    <h1 class="title">Contact Us Today at Capitrek Investments Ltd</h1>
                </div>
            </div>
            <div class="page-breadcrumb position-static">
                <div class="container">
                    <ul class="breadcrumb justify-content-center">
                        <li><a href="/">Home</a></li>
                        <li class="/contact">Contact Us</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Page Title Section End -->

        <!-- Contact Information Section Start -->
        <div class="section section-padding-t90-b100">
            <div class="container shape-animate">
                <!-- Section Title Start -->
                <div class="section-title text-center mb-lg-12 mb-sm-8 mb-xs-8" data-aos="fade-up">
                    <h2 class="title">You want to get more infomation</h2>
                    <p class="sub-title">Our Team of support personnel are waiting for you call.</p>
                </div>
                <!-- Section Title End -->

                <div class="row row-cols-lg-3 row-cols-md-2 row-cols-sm-2 row-cols-1 mb-n6">
                    <div class="col mb-6" data-aos="fade-up">
                        <!-- Contact Information Start -->
                        <div class="contact-info" data-vivus-hover>
                            <div class="icon">
                                <img class="svgInject" src="assets/images/svg/linea/linea-basic-map.svg" alt="">
                            </div>
                            <div class="info">
                                <h4 class="title">Our Locations</h4>
                                <span class="info-text"> Fct Abuja<br>
                                   Wuse 2.</span>
                            </div>
                        </div>
                        <!-- Contact Information End -->
                    </div>
                    <div class="col mb-6" data-aos="fade-up">
                        <!-- Contact Information Start -->
                        <div class="contact-info" data-vivus-hover>
                            <div class="icon">
                                <img class="svgInject" src="assets/images/svg/linea/linea-basic-message-txt.svg" alt="">
                            </div>
                            <div class="info">
                                <h4 class="title">Give Us A Call</h4>
                                <span class="info-text"> (+1) 212-946-2701 <br>
                                    (+1) 212-946-2702</span>
                            </div>
                        </div>
                        <!-- Contact Information End -->
                    </div>
                    <div class="col mb-6" data-aos="fade-up">
                        <!-- Contact Information Start -->
                        <div class="contact-info" data-vivus-hover>
                            <div class="icon">
                                <img class="svgInject" src="assets/images/svg/linea/linea-basic-mail-open-text.svg" alt="">
                            </div>
                            <div class="info">
                                <h4 class="title"> Help Desk</h4>
                                <span class="info-text">
                                    <a href="#">hello@capitrek.com</a>
                                    <br>
                                    <a href="#">sales@capitrek.com</a>
                                </span>
                            </div>
                        </div>
                        <!-- Contact Information End -->
                    </div>
                </div>

                <!-- Animation Shape Start -->
                <div class="shape shape-1 scene">
                    <span data-depth="4"><img src="assets/images/shape-animation/video-shape-1.png" alt="shape"></span>
                </div>
                <!-- Animation Shape End -->

            </div>
        </div>
        <!-- Contact Information Section End -->

        
        <!-- Contact Form Section Start -->
        <div class="contact-form-section section section-padding-t90-b100" data-bg-color="#f8faff">
            <div class="container">
                <div class="row">
                    <div class="offset-lg-2 col-lg-8">
                        <!-- Section Title Start -->
                        <div class="section-title text-center" data-aos="fade-up">
                            <h2 class="title fz-32">Let’s talk about your investment opportuinities</h2>
                          
                        </div>
                        <!-- Section Title End -->
                        <div class="contact-form">
                            <form action="https://htmldemo.hasthemes.com/exomac-preview/exomac/assets/php/contact-mail.php" id="contact-form" method="post">
                                <div class="row mb-n6">
                                    <div class="col-md-6 col-12 mb-6">
                                        <input type="text" placeholder="Your Name *" name="name">
                                    </div>
                                    <div class="col-md-6 col-12 mb-6">
                                        <input type="email" placeholder="Email *" name="email">
                                    </div>
                                    <div class="col-md-12 col-12 mb-6">
                                        <input type="text" placeholder="Subject *" name="subject">
                                    </div>
                                    <div class="col-12 mb-6">
                                        <textarea name="message" placeholder="Message"></textarea>
                                    </div>
                                    <div class="col-12 text-center mb-6">
                                        <button class="btn btn-primary btn-hover-secondary">Submit</button>
                                    </div>
                                </div>
                            </form>
                            <p class="form-messege"></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Contact Form Section End -->


@endsection