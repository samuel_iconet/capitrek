@extends('layouts.authlayout')
@section('content')

<div id="preloader">
        <div class="sk-three-bounce">
            <div class="sk-child sk-bounce1"></div>
            <div class="sk-child sk-bounce2"></div>
            <div class="sk-child sk-bounce3"></div>
        </div>
    </div>

    <div id="main-wrapper">

        <div class="authincation section-padding">
            <div class="container h-100">
                <div class="row justify-content-center h-100 align-items-center">
                    <div class="col-xl-5 col-md-6">
                        <div class="mini-logo text-center my-5">
                            <a href="landing.html"><img src="/assets/images/logo.jpg" alt=""></a>
                        </div>
                        <div class="auth-form card">
                            <div class="card-header justify-content-center">
                                <h4 class="card-title">Reset password</h4>
                            </div>
                            <div class="card-body">
                             @if(isset($user))
                             <form action="" id="savepassword">
                             <input type="hidden" value="{{$user->email}}" id="email">
                            <input type="hidden" value="{{$user->remember_token}}" id="token">
                                    <div class="form-group">
                                        <label>New Password</label>
                                        <input type="password" id="password1" class="form-control" >
                                    </div>
                                    <div class="form-group">
                                        <label>Re-enter Password</label>
                                        <input type="password" id="password2" class="form-control" >
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-success btn-block">Save Password</button>
                                    </div>
                                </form>
                             @else
                             <h3 class="alert alert-danger">Invalid or Expired Link</h3>
                             @endif
                                <div class="new-account mt-3">
                                    <p class="mb-1">Don't Have an Account? </p>
                                    <a class="text-primary" href="/register">Register </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


@endsection
@section('script')

<script type="text/javascript">

    $( document ).ready(function() {
     

    

        $( "#savepassword" ).submit(function(e) {

            e.preventDefault();
       
          let pass1 = $('#password1').val();
          let pass2 = $('#password2').val();
          let token = $('#token').val();
          let email = $('#email').val();
           
            console.log(pass1);
            console.log(pass2);
            if(pass1.length < 6 && pass2.length < 6){
            swal("Error!", "password less than 6 Characters ", "error"); 
               
            }else{
                if(pass1 != pass2){
            swal("Error!", "password Mismatch ", "error"); 

                }else{
                    $.ajaxSetup({
                 headers: { }
             });
            $.post('/api/recoverpassword/save',   // url
                    {    
                           email : email,
                           password : pass1,
                           token : token,
                    }, // data to be submit
                // $.post('/api/token',   // url
                //    {        email: email, 
                //             password: password
                            
                //    }, // data to be submit
                    function(data, status, jqXHR) {// success callback
            
                    console.log(status);
                     if(data.code == 200){
                        swal("Success!", "Password Changed Successfully!! ", "success"); 
                        setTimeout(() => {
                            window.location.href = '/login';
                        }, 3000);

                     }      
                    else if(data.code == 303){
                            swal("Error!", data.error, "error"); 
                        }else{
                            swal("Error!", "Network Error!", "error"); 
                        }
            
                        $('#loader1').hide();
                        $('#submit1').removeAttr('disabled');

                    }).fail(function(jqxhr, settings, ex) {
                        $('#loader1').hide();
                        $('#submit1').removeAttr('disabled');
                        console.log(jqxhr.status);
                        if(jqxhr.status == 400){
                            swal("Error!", "Invalid data!", "error"); 
                        }else{
                            swal("Error!", "Network Error!", "error"); 
                        }
                    });     
                }
            }
            $('#loader1').hide();
            $('#submit1').removeAttr('disabled'); 
        });


 });
   </script>

@endsection