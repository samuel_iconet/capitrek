@extends('layouts.authlayout')
@section('content')

<div id="preloader">
        <div class="sk-three-bounce">
            <div class="sk-child sk-bounce1"></div>
            <div class="sk-child sk-bounce2"></div>
            <div class="sk-child sk-bounce3"></div>
        </div>
    </div>

    <div id="main-wrapper">

        <div class="authincation section-padding">
            <div class="container h-100">
                <div class="row justify-content-center h-100 align-items-center">
                    <div class="col-xl-5 col-md-6">
                        <div class="mini-logo text-center my-5">
                            <a href="landing.html"><img src="/assets/images/logo.jpg" alt=""></a>
                        </div>
                        <div class="auth-form card">
                            <div class="card-header justify-content-center">
                                <h4 class="card-title">Reset password</h4>
                            </div>
                            <div class="card-body">
                                <form action="" id="recoveryform">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" id="recoveryemail" class="form-control" placeholder="hello@example.com">
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-success btn-block">Reset</button>
                                    </div>
                                </form>
                                <div class="new-account mt-3">
                                    <p class="mb-1">Don't Have an Account? </p>
                                    <a class="text-primary" href="/register">Register </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


@endsection

@section('script')
<script>
 $( document ).ready(function(){

$( "#recoveryform" ).submit(function(e) {

e.preventDefault();
$('#main-wrapper').removeClass('show');
 $('#preloader').fadeIn(20);

let email = $('#recoveryemail').val();
if(email == ""){
swal("Error!", "Enter Email ", "error"); 
$('#preloader').fadeOut(1000);
$('#main-wrapper').addClass('show');
}  
else{
$.ajaxSetup({
     headers: { }
 });
$.post('/api/recover/password',   // url
        {    
               email : email
        }, // data to be submit
    // $.post('/api/token',   // url
    //    {        email: email, 
    //             password: password
                
    //    }, // data to be submit
        function(data, status, jqXHR) {// success callback

        console.log(status);
         if(data.code == 200){
                              
            swal("Success!", "Recovery Mail have been sent to your email", "success"); 
            
         }      
        else if(data.code == 401){
                swal("Error!", data.error, "error"); 
            }else{
                swal("Error!", "Network Error!", "error"); 
            }

            $('#preloader').fadeOut(1000);
             $('#main-wrapper').addClass('show');

        }).fail(function(jqxhr, settings, ex) {
           
            console.log(jqxhr.status);
            if(jqxhr.status == 400){
                swal("Error!", "Invalid Email ", "error"); 
            }else{
                swal("Error!", "Network Error!", "error"); 
            }
            $('#preloader').fadeOut(1000);
             $('#main-wrapper').addClass('show');
        });
}   

});

})
</script>

@endsection