<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from demo.quixlab.com/cheerio/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 14 Dec 2020 15:17:39 GMT -->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dashboard | Capitrek </title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <!-- Custom Stylesheet -->
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="/dash/vendor/toastr/toastr.min.css">
    <link rel="stylesheet" href="/dash/css/style.css">
    <script src="/dash/vendor/jquery/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>

</head>

<body>
    <div id="app">
        
    <div id="preloader">
        <div class="sk-three-bounce">
            <div class="sk-child sk-bounce1"></div>
            <div class="sk-child sk-bounce2"></div>
            <div class="sk-child sk-bounce3"></div>
        </div>
    </div>

    <div id="main-wrapper">

        <div class="header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-12">
                        <nav class="navbar">

                            <div class="header-search d-flex align-items-center">
                                <a class="brand-logo mr-3" href="/">
                                    <img src="/assets/images/logo3.jpg" alt="" width="30">
                                </a>
                                
                            </div>


                            <div class="dashboard_log">
                                <div class="d-flex align-items-center">
                                    <div class="profile_log dropdown">
                                        <div class="user" data-toggle="dropdown">
                                            <span class="thumb"><i class="mdi mdi-account"></i></span>
                                            <span class="name" id="userEmail"></span>
                                            <span class="arrow"><i class="la la-angle-down"></i></span>
                                        </div>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="/dashboard" class="dropdown-item">
                                                <i class="mdi mdi-account"></i> Dashboard
                                            </a>
                                            <a href="/account" class="dropdown-item">
                                                <i class="la la-book"></i> Account
                                            </a>
                                            <a href="settings" class="dropdown-item">
                                                <i class="la la-cog"></i> Setting
                                            </a>
                                            
                                            <a class="dropdown-item logout">
                                            <i class="la la-sign-out"></i>
                                                <router-link to="/logout" >
                                                Logout
                                                </router-link>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <div class="sidebar">
            <div >
                <a href="/"><img style="hieght:50px ; width: 100px" src="/assets/images/logo3.jpg" alt="">
                </a>
            </div>
            <div class="menu">
                <ul>
                    <li>
                        <a href="/dashboard" data-toggle="tooltip" data-placement="right" title="Home">
                            <span><i class="icofont-ui-home"></i></span>
                        </a>
                    </li>
                    <!-- <li><a href="buy-sell.html" data-toggle="tooltip" data-placement="right" title="Buy Sale">
                            <span><i class="icofont-stack-exchange"></i></span>
                        </a>
                    </li> -->
                    <li><a href="/account" data-toggle="tooltip" data-placement="right" title="Accounts">
                            <span><i class="icofont-wallet"></i></span>
                        </a>
                    </li>
                    <li><a href="/settings" data-toggle="tooltip" data-placement="right" title="Settings">
                            <span><i class="icofont-ui-settings"></i></span>
                        </a>
                    </li>
                    <li class="logout"><router-link to="/logout"  data-toggle="tooltip" data-placement="right"
                            title="Signout">
                            <span><i class="icofont-power"></i></span>
                        </router-link>
                    </li>
                </ul>

                <p class="copyright">
                    &#169; <a href="#">Capitrek</a>
                </p>
            </div>
        </div>

        <div class="page_title">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="page_title-content">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <router-view></router-view> 


    </div>
</div>

<script src="{{ asset('js/app.js') }}"></script>
    <script src="/dash/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>


    <script src="/dash/vendor/toastr/toastr.min.js"></script>
    <script src="/dash/vendor/toastr/toastr-init.js"></script>

    <script src="/dash/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="/dash/vendor/circle-progress/circle-progress-init.js"></script>


    <!--  flot-chart js -->
    <script src="/dash/vendor/apexchart/apexcharts.min.js"></script>
    <script src="/dash/vendor/apexchart/apexchart-init.js"></script>
    <script src="/dash/vendor/apexchart/apexchart2-init.js"></script>


    <script src="/dash/js/scripts.js"></script>
    <script>
        $(document).ready(function() {
            $('#userEmail').text(localStorage.getItem('email') + " ( " +localStorage.getItem('name') + " )");
            $('#button').click( (e) => {
                e.preventDefault();
     var options = {
        series: [76, 67, 61, 90],
        chart: {
            height: 300,
            type: 'radialBar',
        },
        tooltip: {
            enabled: true,
        },
        plotOptions: {
            radialBar: {
                offsetY: 0,
                startAngle: 0,
                endAngle: 360,
                hollow: {
                    margin: 5,
                    size: '20%',
                    background: 'transparent',
                    image: undefined,
                },
                dataLabels: {
                    name: {
                        show: false,
                    },
                    value: {
                        show: false,
                    }
                }
            }
        },
        colors: [
            'rgba(137, 22, 255,1)',
            'rgba(137, 22, 255,0.7)',
            'rgba(137, 22, 255,0.3)',
            'rgba(137, 22, 255,0.1)'
        ],
        labels: ['Bitcoin', 'Litecoin', 'Ripple', 'Dash'],
        legend: {
            show: false,
            floating: true,
            fontSize: '16px',
            position: 'left',
            offsetX: 160,
            offsetY: 15,
            labels: {
                useSeriesColors: true,
            },
            markers: {
                size: 0
            },
            formatter: function (seriesName, opts) {
                return seriesName + ":  " + opts.w.globals.series[opts.seriesIndex]
            },
            itemMargin: {
                vertical: 3
            }
        },
        responsive: [{
            breakpoint: 480,
            options: {
                legend: {
                    show: false
                }
            }
        }]
    };

    var chart = new ApexCharts(document.querySelector("#wallet-chart"), options);
    chart.render();

                //   $('#main-wrapper').removeClass('show');
                // $('#preloader').fadeIn(20);
                // $('#preloader').fadeOut(10000);
                // $('#main-wrapper').addClass('show');
                // toastr.success("Complete your profile to make it easier to exchange", "Complete your profile!", {
                //         // timeOut: 500000,
                //         closeButton: !0,
                //         debug: !1,
                //         newestOnTop: !0,
                //         progressBar: !0,
                //         positionClass: "toast-top-right demo_rtl_class",
                //         preventDuplicates: !0,
                //         onclick: null,
                //         showDuration: "300",
                //         hideDuration: "1000",
                //         extendedTimeOut: "1000",
                //         showEasing: "swing",
                //         hideEasing: "linear",
                //         showMethod: "fadeIn",
                //         hideMethod: "fadeOut",
                //         tapToDismiss: !1,
                //         closeHtml: '<div class="circle_progress"></div><span class="progress_count"></span> <i class="la la-close"></i> <a href="#">Suggest</a>'
                //     });
            });
            function preload(){
                // $('#main-wrapper').removeClass('show');
                // $('#preloader').fadeOut(500);
                // $('#main-wrapper').addClass('show');

                
            }
        } );
    </script>
</body>


<!-- Mirrored from demo.quixlab.com/cheerio/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 14 Dec 2020 15:17:47 GMT -->
</html>