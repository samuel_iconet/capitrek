<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from demo.quixlab.com/cheerio/signin.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 14 Dec 2020 15:17:53 GMT -->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="apple-touch-icon" sizes="57x57" href="/assets/images/icons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/assets/images/icons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/assets/images/icons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/assets/images/icons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/assets/images/icons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/assets/images/icons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/assets/images/icons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/assets/images/icons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/assets/images/icons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/assets/images/icons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/images/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/assets/images/icons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/images/icons/favicon-16x16.png">
    <link rel="manifest" href="/assets/images/icons/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/assets/images/icons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Capitrek  </title>
    <!-- Favicon icon -->
  
    <!-- Custom Stylesheet -->
    
    
    <link rel="stylesheet" href="/dash/css/style.css">
    
    <link rel="stylesheet" href="sweetalert2.min.css">
    @yield('style')
</head>

<body>

@yield('content')

    <script src="/dash/vendor/jquery/jquery.min.js"></script>
    <script src="/dash/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    

    <script src="/dash/vendor/validator/jquery.validate.js"></script>
    <script src="/dash/vendor/validator/validator-init.js"></script>

    <script src="/dash/js/scripts.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    @yield('script')
    
    
</body>


<!-- Mirrored from demo.quixlab.com/cheerio/signin.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 14 Dec 2020 15:17:54 GMT -->
</html>