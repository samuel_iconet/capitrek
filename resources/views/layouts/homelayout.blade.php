<!DOCTYPE html>
<html class="no-js" lang="en">


<!-- Mirrored from htmldemo.hasthemes.com/exomac-preview/exomac/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 14 Dec 2020 14:05:54 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Capitrek Investments</title>
    <meta name="robots" content="noindex, follow" />
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="/assets/images/icons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/assets/images/icons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/assets/images/icons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/assets/images/icons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/assets/images/icons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/assets/images/icons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/assets/images/icons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/assets/images/icons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/assets/images/icons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/assets/images/icons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/images/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/assets/images/icons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/images/icons/favicon-16x16.png">
    <link rel="manifest" href="/assets/images/icons/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/assets/images/icons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- CSS
	============================================ -->

    <!-- Vendor CSS (Bootstrap & Icon Font) -->
    <!-- <link rel="stylesheet" href="/assets/css/vendor/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/vendor/font-awesome-pro.min.css">
    <link rel="stylesheet" href="/assets/css/vendor/pe-icon-7-stroke.css">
    <link rel="stylesheet" href="/assets/css/vendor/muli-font.css"> -->

    <!-- Plugins CSS (All Plugins Files) -->
    <!--
    <link rel="stylesheet" href="/assets/css/plugins/swiper.min.css">
    <link rel="stylesheet" href="/assets/css/plugins/animate.css">
    <link rel="stylesheet" href="/assets/css/plugins/aos.min.css">
    <link rel="stylesheet" href="/assets/css/plugins/magnific-popup.css">
     -->

    <!-- Main Style CSS -->
    <!-- <link rel="stylesheet" href="/assets/css/style.css"> -->

    <!-- Use the minified version files listed below for better performance and remove the files listed above -->
    <link rel="stylesheet" href="/assets/css/vendor/vendor.min.css">
    <link rel="stylesheet" href="/assets/css/plugins/plugins.min.css">
    <link rel="stylesheet" href="/assets/css/style.min.css">

</head>

<body>

    <div id="page" class="section">
        <!-- Header Section Start -->
        <div class="header-section header-transparent sticky-header section">
            <div class="header-inner">
                <div class="container position-relative">
                    <div class="row justify-content-between align-items-center">

                        <!-- Header Logo Start -->
                        <div class="col-xl-2 col-auto order-0">
                            <div class="header-logo">
                                <a href="index.html">
                                    <img class="dark-logo" src="/assets/images/logo2.jpg" alt="Agency Logo">
                                    <img class="light-logo" src="/assets/images/logo2.jpg" alt="Agency Logo">
                                </a>
                            </div>
                        </div>
                        <!-- Header Logo End -->

                        <!-- Header Main Menu Start -->
                        <div class="col-auto col-xl d-flex align-items-center justify-content-xl-center justify-content-end order-2 order-xl-1">
                            <div class="menu-column-area d-none d-xl-block position-static">
                                <nav class="site-main-menu">
                                    <ul>
                                        <li>
                                            <a href="/"><span class="menu-text">Home</span></a>
                                        </li>
                                        
                                        
                                        <li >
                                            <a href="blog-grid.html"><span class="menu-text">News</span></a>
                                           
                                        </li>
                                       
                                        <li>
                                            <a href="/contact"><span class="menu-text">Contact Us</span></a>
                                        </li>
                                        <li>
                                            <a href="/login"><span class="menu-text">Login</span></a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                            <!-- Header Search Start -->
                            <div class="header-search-area ml-xl-7 ml-0">

                                <!-- Header Login Start -->
                                <div class="header-search">
                                    <a href="javascript:void(0)" class="header-search-toggle"><i class="pe-7s-search pe-2x pe-va"></i></a>
                                </div>
                                <!-- Header Login End -->
                            </div>
                            <!-- Header Search End -->

                            <!-- Header Mobile Menu Toggle Start -->
                            <div class="header-mobile-menu-toggle d-xl-none ml-sm-2">
                                <button class="toggle">
                                    <i class="icon-top"></i>
                                    <i class="icon-middle"></i>
                                    <i class="icon-bottom"></i>
                                </button>
                            </div>
                            <!-- Header Mobile Menu Toggle End -->
                        </div>
                        <!-- Header Main Menu End -->

                        <!-- Header Right Start -->
                        <div class="col-xl-2 col d-none d-sm-flex justify-content-end order-1 order-xl-2">
                            <a href="/register" class="btn btn-light btn-hover-primary">Join Now</a>
                        </div>
                        <!-- Header Right End -->

                    </div>
                </div>
            </div>
        </div>
        <!-- Header Section End -->

        <!-- Main Search Start -->
        <div class="main-search-active">
            <div class="sidebar-search-icon">
                <button class="search-close"><i class="pe-7s-close"></i></button>
            </div>
            <div class="sidebar-search-input">
                <form action="#">
                    <div class="form-search">
                        <input id="search" class="input-text" value="" placeholder="" type="search">
                        <button>
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                </form>
                <p class="form-description">Hit enter to search or ESC to close</p>
            </div>
        </div>
        <!-- Main Search End -->
        @yield('content')

        <div class="footer-section section" data-bg-color="#030e22">
            <div class="container">

                <!-- Footer Top Widgets Start -->
                <div class="row mb-lg-14 mb-md-10 mb-6">

                    <!-- Footer Widget Start -->
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 col-12 mb-6">
                        <div class="footer-widget">
                            <div class="footer-logo">
                                <a href="index.html"><img src="/assets/images/logo.jpg" alt="Agency Logo"></a>
                            </div>
                            <div class="footer-widget-content">
                                <div class="content">


                                    <p><a href="#">(+1) 212-946-2701</a></p>
                                    <p><a href="#">info@capitrek.com</a> </p>
                                </div>
                                <div class="footer-social-inline">
                                    <a href="#"><i class="fab fa-twitter-square"></i></a>
                                    <a href="#"><i class="fab fa-facebook-square"></i></a>
                                    <a href="#"><i class="fab fa-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Footer Widget End -->

                    <!-- Footer Widget Start -->
                    <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-6 mb-6">
                        
                    </div>
                    <!-- Footer Widget End -->

                    <!-- Footer Widget Start -->
                    <div class="col-xl-2 col-lg-2 col-md-4 col-sm-4 col-6 mb-6">
                        
                    </div>
                    <!-- Footer Widget End -->

                    <!-- Footer Widget Start -->
                    <div class="col-xl-2 col-lg-2 col-md-4 col-sm-4 col-6 mb-6">
                       
                    </div>
                    <!-- Footer Widget End -->

                    <!-- Footer Widget Start -->
                    <div class="col-xl-2 col-lg-2 col-md-4 col-sm-4 col-6 mb-6">
                        
                    </div>
                    <!-- Footer Widget End -->

                </div>
                <!-- Footer Top Widgets End -->

                <!-- Footer Copyright Start -->
                <div class="row">
                    <div class="col">
                        <p class="copyright">Copyright &copy; 2020 <a href="https://hasthemes.com/">Capitrek</a>. All Rights Reserved.</p>
                    </div>
                </div>
                <!-- Footer Copyright End -->

            </div>
        </div>

        <!-- Scroll Top Start -->
        <a href="#" class="scroll-top" id="scroll-top">
            <i class="arrow-top fal fa-long-arrow-up"></i>
            <i class="arrow-bottom fal fa-long-arrow-up"></i>
        </a>
        <!-- Scroll Top End -->
    </div>




    <!-- JS
============================================ -->

    <!-- Vendors JS -->
    <!-- <script src="/assets/js/vendor/modernizr-3.6.0.min.js"></script>
<script src="/assets/js/vendor/jquery-3.4.1.min.js"></script>
<script src="/assets/js/vendor/jquery-migrate-3.1.0.min.js"></script>
<script src="/assets/js/vendor/bootstrap.bundle.min.js"></script> -->

    <!-- Plugins JS -->
    <!-- <script src="/assets/js/plugins/parallax.min.js"></script>
<script src="/assets/js/plugins/jquery.ajaxchimp.min.js"></script> -->

    <!-- Use the minified version files listed below for better performance and remove the files listed above -->
    <script src="/assets/js/vendor/vendor.min.js"></script>
    <script src="/assets/js/plugins/plugins.min.js"></script>

    <!-- Main Activation JS -->
    <script src="/assets/js/main.js"></script>





</body>


<!-- Mirrored from htmldemo.hasthemes.com/exomac-preview/exomac/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 14 Dec 2020 14:06:13 GMT -->
</html>