@extends('layouts.mailLayout')
@section('content')
<tr>
    <td bgcolor="#333333">
        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr>
                <td style="padding: 30px 40px 30px 40px; text-align: center;"> <span style="color:#fff; font-size: 30px">Capitrek.com</span> </td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td background="https://res.cloudinary.com/dxfq3iotg/image/upload/v1556165136/switzerland-862870_1920.jpg" bgcolor="#222222" align="center" valign="top" style="text-align: center; background-position: center center !important; background-size: cover !important;">
        <div>
            <table role="presentation" border="0" cellpadding="0" cellspacing="0" align="center" width="100%" style="max-width:500px; margin: auto;">
                <tr>
                    <td height="20" style="font-size:20px; line-height:20px;">&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" valign="middle">
                        <table>
                            <tr>
                                <td valign="top" style="text-align: center; padding: 60px 0 10px 20px;">
                                    <h1 style="margin: 0; font-family: 'Montserrat', sans-serif; font-size: 30px; line-height: 36px; color: #ffffff; font-weight: bold;">Hello  {{$user->name}},</h1>
                                </td>
                            </tr>
                            
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="20" style="font-size:20px; line-height:20px;">&nbsp;</td>
                </tr>
            </table>
        </div>
    </td>
</tr>
<tr>
    <td bgcolor="#ffffff">
        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr>
                <td style="padding: 40px 40px 20px 40px; text-align: left;">
                    <h1 style="margin: 0; font-family: 'Montserrat', sans-serif; font-size: 20px; line-height: 26px; color: #333333; font-weight: bold;">YOUR ACCOUNT WAS CREATED SUCCESSFULL</h1>
                </td>
            </tr>
            <tr>
                <td style="padding: 0px 40px 20px 40px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555; text-align: left; font-weight:bold;">
                    <p style="margin: 0;">Thank you for registering with Capitrek.</p>
                    <p style="margin: 0;">Please follow the link below to activate account</p>
                </td>
            </tr>
            <tr>
                <td style="padding: 0px 40px 20px 40px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555; text-align: left; font-weight:normal;">
                  <?php $url = url('/') ."/api/activate" .'/' .$user->id .'/'.$user->remember_token ?>
                    <!-- <a href="{{$url}}" class="btn btn-success btn-sm">Activate Account</a> -->
                    {{$url }}
                 </td>
            </tr>
        </table>
    </td>
</tr> <!-- INTRO : END -->
<!-- CTA : BEGIN -->


@endsection