@extends('layouts.authlayout')
@section('content')
<div id="preloader">
        <div class="sk-three-bounce">
            <div class="sk-child sk-bounce1"></div>
            <div class="sk-child sk-bounce2"></div>
            <div class="sk-child sk-bounce3"></div>
        </div>
    </div>

    <div id="main-wrapper">

        <div class="authincation section-padding">
            <div class="container h-20">
                <div class="row justify-content-center h-20 align-items-center">
                    <div class="col-xl-5 col-md-6">
                            <div class="mini-logo text-center my-5">
                                <a href="landing.html"><img src="/assets/images/logo.jpg" alt=""></a>
                            </div>
                        <div class="auth-form card">
                            <div class="card-header justify-content-center">
                                <h4 class="card-title">Sign in</h4>
                            </div>
                            <div class="card-body">
                                <form method="post" id="submit" class="signin_validate" >
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" required class="form-control" placeholder="hello@capitrek.com"
                                            id="email">
                                    </div>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" required class="form-control" placeholder="Password"
                                            id="password">
                                    </div>
                                    <div class="form-row d-flex justify-content-between mt-4 mb-2">
                                        <div class="form-group mb-0">
                                            <label class="toggle">
                                                <input class="toggle-checkbox" type="checkbox">
                                                <div class="toggle-switch"></div>
                                                <span class="toggle-label">Remember me</span>
                                            </label>
                                        </div>
                                        <div class="form-group mb-0">
                                            <a href="/reset" >Forgot Password?</a>
                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit"  class="btn btn-success btn-block">Sign in</button>
                                    </div>
                                </form>
                                <div class="new-account mt-3">
                                    <p>Don't have an account? <a class="text-primary" href="/register">Sign
                                            up</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

@endsection

@section('script')

<script type="text/javascript">

    $( document ).ready(function() {
     // $( "#err" ).hide();
      let token = "Bearer "  + localStorage.getItem('access_token');
      console.log(token);
     $.ajax({
     type: "GET",
     headers: {'Content-Type': 'application/json', Authorization: token  },
     url: "/api/validateSession",
     success: function(patientDTO) {
         console.log("SUCCESS: ", patientDTO);
         if(patientDTO.code == '200'){
            window.location.href = '/dashboard';
         }else{

         }
         
          },
     error: function(e) {
     console.log("ERROR: ", e);
     e.preventDefault;
    
     }
 });

   
   


    $( "#submit" ).submit(function(e) {
                $('#main-wrapper').removeClass('show');
                $('#preloader').fadeIn(20);
                // $('#preloader').fadeOut(10000);
                // $('#main-wrapper').addClass('show');
         e.preventDefault();
         
     let email = $('#email').val();
     let password = $('#password').val();
     let token2 = '';
     console.log(email);
     console.log(password);
     if(email == ""){
            swal("Error!", "Enter Email ", "error"); 
            $('#preloader').fadeOut(1000);
             $('#main-wrapper').addClass('show');
           }  
           else  if(password == ""){
            swal("Error!", "Enter Password ", "error");
            $('#preloader').fadeOut(1000);
           $('#main-wrapper').addClass('show');
           }else{
            $.ajaxSetup({
                 headers: { }
             });
 $.post('/oauth/token',   // url
        {        username: email, 
                 password: password,
                 grant_type: "password",
                 client_id: "2",
                 client_secret: "zolOHNlDNwc3wN5hRFw0T8IWB0pAoycXISJu3ryu"
        }, // data to be submit
     // $.post('/api/token',   // url
     //    {        email: email, 
     //             password: password
                
     //    }, // data to be submit
        function(data, status, jqXHR) {// success callback
 
         console.log(status);
                // console.log(data.access_token);
               localStorage.setItem('access_token', data.access_token);
                token  = data.access_token;
                   console.log(token);
                  token = "Bearer "  + token;
                   console.log(token);
                   $.ajax({
                    type: "GET",
                    headers: {'Content-Type': 'application/json', Authorization: token  },
                    url: "/api/validateSession",
                    success: function(patientDTO) {
                        console.log(patientDTO.code);
                        $('#preloader').fadeOut(1000);
                       $('#main-wrapper').addClass('show');
                        if(patientDTO.code == '200'){
                            console.log("SUCCESS: ", patientDTO);
                                  localStorage.setItem('name', patientDTO.user.name);
                                  localStorage.setItem('email', patientDTO.user.email);
                                  localStorage.setItem('id', patientDTO.user.id);
                                  localStorage.setItem('role', patientDTO.user.role);
                                  localStorage.setItem('profile', patientDTO.user.profile);
                                  localStorage.setItem('status', patientDTO.user.status);
                                  localStorage.setItem('created_at', patientDTO.user.created_at);
                                  window.location.href = '/dashboard';
                        }else{
                            swal("Error!", "Your Account has not yet being activated, Please check you mail for activation link.", "error"); 
                      
                        }
                                  
                        },
                    error: function(e) {
                        $('#preloader').fadeOut(1000);
                     $('#main-wrapper').addClass('show');
                    console.log("ERROR: ", e);
                    e.preventDefault;
                    swal("Error!", "Your Account has not yet being activated, Please check you mail for activation link.", "error"); 
                    }
                });
                
 
         }).fail(function(jqxhr, settings, ex) {
                 $('#preloader').fadeOut(100);
                $('#main-wrapper').addClass('show');
            console.log(jqxhr.status);
            if(jqxhr.status == 400){
                swal("Error!", "Invalid Email and Password Provided!", "error"); 
            }else{
                swal("Error!", "Network Error!", "error"); 
            }
          });
           }

 
 
 
 
 });
 });
   </script>

@endsection