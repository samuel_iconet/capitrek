
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import Vue from 'vue/dist/vue.common.js'

import VueSweetalert2 from 'vue-sweetalert2';
 
import 'sweetalert2/dist/sweetalert2.min.css';
import VueApexCharts from 'vue-apexcharts'
import VueRouter from 'vue-router';

Vue.use(VueSweetalert2);


window.Vue = Vue;
Vue.use(VueRouter);
Vue.use(VueApexCharts)

// import VueRouter from 'vue-router';
// import { Form, HasError, AlertError } from 'vform';
// import VueSweetalert2 from 'vue-sweetalert2';
 
 //import Datetime from 'vue-datetime';
//import 'vue-datetime/dist/vue-datetime.css';
//  import datePicker from 'vue-bootstrap-datetimepicker';
//   import 'pc-bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.css';
//   Vue.use(datePicker);
// window.datePicker = datePicker;



// Vue.use(Datetime);
// window.Datetime = Datetime;
const options = {
  confirmButtonColor: '#41b882',
  cancelButtonColor: '#ff7674'
}
 
// Vue.use(VueSweetalert2, options);

// window.axios = require('axios');
// window.swal = VueSweetalert2;
// window.Form = Form;
// window.email = "";
// window.username = "";

// window.Fire = Fire;
// Vue.use(VueRouter)
// Vue.config.devtools = false;

let Fire = new Vue();
window.Fire = Fire;



let routes = [
 { path: '/dashboard', component: require('./components/dashboard.vue').default },
 { path: '/userdashboard', component: require('./components/userdashboard.vue').default },
 { path: '/account', component: require('./components/account.vue').default },
 { path: '/settings', component: require('./components/settings.vue').default },
 { path: '/ticket', component: require('./components/ticket.vue').default },
 { path: '/logout', component: require('./components/logout.vue').default },
 
 { path: '*', component: require('./components/404.vue').default  }
 
]

Vue.component(
    'userbalance',
    require('./components/userbalance.vue').default
);
Vue.component('apexchart', VueApexCharts)

const router = new VueRouter({
	mode: 'history',
  routes // short for `routes: routes`
})

const app = new Vue({

    el: '#app',
    router
});
